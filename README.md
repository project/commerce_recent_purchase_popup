## INTRODUCTION ##

Author and maintainer: **Andrii Sakhaniuk**
 * Drupal: https://www.drupal.org/u/nnevill

Sponsored by: **DevBranch**
 * Drupal : https://www.drupal.org/devbranch
 * Website : https://dev-branch.com/

The module provides block plugin which display latest product purchased on the website.

## INSTALLATION ##

See https://www.drupal.org/documentation/install/modules-themes/modules-8
for instructions on how to install or update Drupal modules.

## CONFIGURATION ##

* Open /admin/structure/block on your Drupal installation, click "Place block" for e.g. Content region and add block with name "Recent Purchase Popup block
".
* Configure it.
* Open e.g. homepage to check if it works.

### PERMISSIONS ###

There are no specific permissions created for this module but as far as module provides block access could be controlled on block level.
